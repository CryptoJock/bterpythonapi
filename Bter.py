# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Acknowledgements: https://github.com/ScriptProdigy/CryptsyPythonAPI
# was used as reference for this module

import urllib
import urllib2
import json
import hmac, hashlib

BROWSER_AGENT = 'Mozilla/5.0'

class Bter:
    def __init__(self, APIKey, Secret):
        self.APIKey = APIKey
        self.Secret = Secret
        self.spoof_hdr = {'User-Agent': BROWSER_AGENT}
    def api_query(self, method, req={}):
        if method == "pairs" or method == "tickers":
            ret = urllib2.urlopen(urllib2.Request('http://data.bter.com/api/1/%s' % (method), headers=self.spoof_hdr))
            return json.loads(ret.read())
        elif method == "ticker" or method == "depth" or method == "trade" :
            ret = urllib2.urlopen(urllib2.Request('http://data.bter.com/api/1/%s/%s' % (method, req['pair']), headers=self.spoof_hdr))
            return json.loads(ret.read())
        else:
            post_data = urllib.urlencode(req)

            sign = hmac.new(self.Secret, post_data, hashlib.sha512).hexdigest()
            headers = {
                'Sign': sign,
                'Key': self.APIKey,
                'User-Agent': BROWSER_AGENT
            }

            ret = urllib2.urlopen(urllib2.Request('https://bter.com/api/1/private/%s' % (method), post_data, headers))
            return json.loads(ret.read())

    def getPairs(self):
        return self.api_query('pairs', {})

    def getTickers(self):
        return self.api_query('tickers', {})

    def getTicker(self, pair):
        return self.api_query('ticker', {'pair' : pair})

    def getDepth(self, pair):
        return self.api_query('depth', {'pair' : pair})

    def getTradeHistory(self, pair):
        return self.api_query('trade', {'pair' : pair})

    def myFunds(self):
        return self.api_query('getfunds', {})
    
    def myOrders(self):
        return self.api_query('getorder', {})


    def placeOrder(self, pair, ordertype, quantity, price):
        return self.api_query('placeorder', {'pair' : pair, 'type' : ordertype, 'amount' : quantity, 'rate' : price})

    def cancelOrder(self, order_id):
        return self.api_query('cancelorder', {'order_id' : order_id})
